# Smart Automatic Chicken Door

Adding a smart plug and a z-wave door/window sensor to an automatic chicken door.  Then setting them up with automation in SmartThings and Alexa to automatically open each morning based on sunrise and close each evening based upon sunset.

[Automatic Chicken Coop Door](https://www.amazon.com/AutomaticChickenCoopDoor-com-Automatic-Chicken-Coop-Door/dp/B06VVPF329/ref=sr_1_3?dchild=1&keywords=AutomaticChickenCoopDoor.com&qid=1602460301&sr=8-3)

[TP-Link Kasa Smart Plug](https://www.amazon.com/TP-LINK-HS103P2-Required-Google-Assistant/dp/B07B8W2KHZ/ref=sr_1_5?dchild=1&keywords=kasa+smart+plug&qid=1602460377&sr=8-5)

[BeSense ZWave Door and Windows Sensor](https://www.amazon.com/BeSense-Sensor-Natively-Samsung-SmartThings/dp/B073RV9VYC/ref=redir_mobile_desktop?ie=UTF8&aaxitk=DWxJPVstWOHPvXeHvFO-dw&hsa_cr_id=7714062900801&ref_=sbx_be_s_sparkle_mcd_asin_0)

[8.5 x 11 Acrylic Picture/Sign Holder](https://www.amazon.com/MaxGear-Acrylic-Plastic-Holders-Business/dp/B07R556S8P/ref=sr_1_17?dchild=1&keywords=acrylic+picture+frame+8x10&qid=1602460544&sr=8-17)

